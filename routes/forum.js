var express = require('express');
var router = express.Router();
var passport = require('./auth.js');
var mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var multer = require('multer');
var nodemailer = require('nodemailer');
var async = require('async');
var crypto = require('crypto');
var users = mongoose.model('users');
var skills = mongoose.model('skills');
var questions = mongoose.model('questions');
var answers = mongoose.model('answers');
var comments = mongoose.model('comments');
var upvotes = mongoose.model('upvotes');


var website = 'localhost:3000'
// create reusable transporter object using SMTP transport

var transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
        user: 'nikhil271995',
        pass: 'nik271995'
    }
});

/* GET home page. */
router.get('/', function(req, res, next) {
  questions.find({},function(err,data){
    res.render('forum/questions.html',{'user':req.session.user});
  });
});

router.get('/newQuestion', userValidate , function(req,res,next){
	res.render('forum/question_new.html',{'user':req.session.user});
});

router.get('/question/:id', userValidate , function(req,res,next){
  console.log(req.params.id,"id");
  questions.findById(req.params.id,function(err,data){
    if(err)
    {
      console.log(err);
    }
    // console.log(data,"\nData");
    res.render('forum/single.html',{'user':req.session.user,'data':data});
  });
});

router.post('/questionData',userValidate,function(req,res,next){
  questions.findById(req.body.id).populate('askedBy answers.answer answers.user').exec(function(err,data){
  res.send(data);
  });
});

router.post('/answerUpvoteStatus/:ansid/:id',userValidate,function(req,res,next){
  console.log("i am here");
  upvotes.find({'user':req.session.user._id.toString(),"answer":req.params.ansid,"status":"true" }).populate("answer user").exec(function(err,data){
    console.log(data.length);
    if(data.length>0)
      res.send({"res":true});
    else
      res.send({"res":false});
  });
});

router.post('/data/:id',function(req,res,next){
  console.log("i am here");
  var skip = req.params.id-3;
  if(skip<0)
    skip=0;
  questions.find({}).skip(skip).limit(3).populate('askedBy answers.user answers.answer').exec(function(err,data){
    console.log(data);
  res.send(data);
  });
});

router.get('/upvote/:ansid/:id',userValidate,function(req,res,next){
    var link = req.header('Referer');
    console.log(link);
    upvotes.find({"user":req.session.user._id,"answer":req.params.ansid},function(err,data)
    {
      console.log(data,"intial data");
      if(!data.length)
      {
        console.log("New upvote");
        var upvote = new upvotes({
         answer:req.params.ansid,
         question:req.params.id,
         user : req.session.user._id.toString(),
         status : true
        });
        upvote.save(function(err, upvote){
        console.log(upvote);
          if(err)
            {
              console.log(err);
              res.send("Server Error");
            }
          else {
             if(data) {
                answers.findByIdAndUpdate(req.params.ansid,{
                  $inc : {
                    upvotes :1
                  }
                },function(err,questionData){
              });
                users.findByIdAndUpdate(req.session.user._id,{
                  $push : {
                    upvotes : upvote._id.toString()
                  }
                },function(err,upvotedata){
                });
            res.send({"code":"success","ansid":req.params.ansid,"id":req.params.id});
            }
          }
         });
      }
      else {
        upvotes.update({"user":req.session.user._id,"answer":req.params.ansid},{
          $set : {
            status : true
          }
        },function(err,newData){
          answers.findByIdAndUpdate(req.params.ansid,{
              $inc : {
                upvotes :1
              }
            },function(err,questionData){
            });
          console.log("i am here newData",newData);
        });
         res.send({"code":"success","ansid":req.params.ansid,"id":req.params.id});
      }
    });
});

router.get('/downvote/:ansid/:id',userValidate,function(req,res,next){
    var link = req.header('Referer');
    console.log(link);
    // console.log(req.session.user._id);
    console.log(req.session.user._id," session  ansid    ",req.params.ansid);
    upvotes.find({"user":req.session.user._id,"_id":req.params.id},function(err,data){
      console.log(data,err,"downvote");
    })
    upvotes.update({"user":req.session.user._id,"answer":req.params.ansid},{
      $set : 
      {
            status : false
      }
    },function(err,data)
    {
      console.log(data,"data",err);
        if(data.nModified){
          answers.findByIdAndUpdate(req.params.ansid,{
            $inc : {
              upvotes :-1
            }
          },
          function(err,questionData){
             res.send({"code":"success","ansid":req.params.ansid,"id":req.params.id});
          });
        }
        else
        res.send("failure");
    });
});

router.post('/question/addAnswer',userValidate, function(req,res,next){
  console.log(req.body,"req");
  var link = req.header('Referer');
  var address = link.split('/');
  console.log(address,"address",address.length);
  var id = address[5];
  console.log("id",id);
  var answer = new answers({
        user :   req.session.user._id.toString(),
        answer : req.body.answer,
        time : moment().format('MMMM Do YYYY, h:mm:ss a'),
        question : id,
        upvotes : 0
      });
  answer.save(function(err,data){
    if(err)
      console.log(err);
      questions.findByIdAndUpdate(id,
      {
        $push : {
          answers : {
            answer : data._id.toString(),
            user : req.session.user._id
          }
        }
      },
      {
        safe: true,
        upsert: true,
        new : true
      },
      function(err,question){
        console.log(question,"question");
        if(err)
        {
          console.log(err);
          res.send({"error":"error"});
        }
        else{
          users.findByIdAndUpdate(req.session.user._id,{
            $push : {
              answers : data._id.toString()
            }
          },function(err,userData){})
          res.send({"success":"success"});
        }
      }
    )
  })
});

router.post('/newQuestion',userValidate, function(req,res,next){
  if(req.body)
  {
      var question = new questions({
			question : req.body.question,
			details : req.body.detail,
			topics : req.body.topics,
      mode : req.body.mode,
			time : moment().format('MMMM Do YYYY, h:mm:ss a'),
			askedBy : req.session.user._id.toString()
		});
    question.save(function(err, question) {
			if (err){
				console.log(err);
				req.flash('error','Database Error. Please Try again or Contact Admin if it persists.');
				res.redirect('/forum/newQuestion');
			}
			else{
        console.log(question);
        req.flash('success','successfully inserted.');
        res.redirect('/forum');
      }
    });
  }
});

// router.get("/upvote/:ansid/:id",userValidate,function(req,res,next){
//   answers.findByIdAndUpdate(req.params.ansid,{
//     $inc : {
//       upvotes :1 
//     }
//   },function(err,data){
//     if(err)
//       console.log(err);
//      var upvote = new upvotes({
//         user :   req.session.user._id.toString(),
//         answer : req.params.ansid,
//         status   : true,
//       });
//   })
// })

router.get('/viewUsers', userValidate , function(req,res,next){
  users.find({}).populate('skills').exec(function(err,userData){
    res.render('mentors/viewUsers.html',{'user':req.session.user,'users':userData});
  })
});

router.get('/viewSkills', userValidate , function(req,res,next){
  skills.find({}).populate('skills').exec(function(err,userData){
    res.render('mentors/viewSkills.html',{'user':req.session.user,'users':userData});
  })
});

var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

function userValidate(req,res,next){
	//console.log(req.user);
	users.findById(req.user,function(err, user) {
		if(user!=null){
			req.session.user = user;
      res.locals.user=user;
			next();
		}
		else {
      		res.redirect('/users/login');
		}
	});
}

module.exports = router;
