var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var userSchema = new Schema({
	first_name:{
		type: String,
	},
	last_name:{
		type: String,
	},
	name :{
		type:String,
		required : true
	},
	email:{
		type: String,
		required: true,
		unique: true
	},
	altEmail:{
		type: String,
		unique : false,
	},
	description: {
		type:String,
		unique:false,
		default:""
	},
	lastLogin: {
		type: String,
		unique : false,
		default : "Never"
	},
	password:{
		type: String,
		required: true,
	},
	phone:{
		mobile:Number,
		isd:Number,
		std:Number,
		landline:Number
	},
	dob:String,
	age:{
		type: Number,
		default:""
	},
	address:{
		type:String
	},
	facebook:String,
	twitter:String,
	linkedin:String,
	github:String,
	about:String,
	blog:String,
	skype:String,
	maritial:String,
	gender:String,
	profile:{
		name:String,
		original_name:String
	},
	summary:String,
	resume:{
		name:String,
		original_name:String
	},
	tenth:{
		school_name:String,
		city:String,
		state:String,
		board:String,
		marks:String,
		year:String,
		medium:String,
	},
	twelfth : {
		school_name:String,
		city:String,
		state:String,
		board:String,
		marks:String,
		year:String,
		medium:String,
		specialization:String,
		mode:String
	},
	grad:{
		school_name:String,
		city:String,
		state:String,
		board:String,
		marks:String,
		year:String,
		medium:String,
		specialization:String,
		mode:String
	},
	postgrad : {
		school_name:String,
		city:String,
		state:String,
		board:String,
		marks:String,
		year:String,
		medium:String,
		specialization:String,
		mode :String
	},
	_email:{
		type:Boolean,
		required:true,
		default:false
	},
	_approved:{
		type:Boolean,
		required:true,
		default:false
	},
	_login:{
		type:Boolean,
		default:true
	},
	resetPasswordToken : {
		type : String
	},
	resetPasswordExpires : {
		type : Date
	},
	learningSkills:[{
			type: Schema.Types.ObjectId,
			ref: 'skills'
	}],
	purchasedSkills:[{
			type: Schema.Types.ObjectId,
			ref: 'skills'
	}],
	finishedSkills:[{
			type: Schema.Types.ObjectId,
			ref: 'skills'
	}],
	_isMentor : {
		type :Boolean,
		default : "false"
	},
	_isMentorId :{
		type : Boolean,
		default : false
	},
	mentorId : {
		type : String,
		default : null
	},
	mentorSkills : [{
		skillRating : {
			type : Number,
			default :"0"
		},
		skillId : {
			type: Schema.Types.ObjectId,
			ref: 'skills'
		}
	}],
	image : {
		type:String,
		default : "default.png"
	},
	_isAdmin :{
		type: Boolean,
		default :false
	},
	blog : [{
		type : Schema.Types.ObjectId,
		ref:'posts'
	}],
	messageCount :{
		type :Number ,
		default :0
	},
	message : [{
		text :String,
		time :String,
		notes : String ,
		qualification : String,
		school : String
	}],
	education : [{
		type :Schema.Types.ObjectId,
		ref : 'usereducations'
	}],
	username :String,
	mentor1:{
		shortBio:String,
		miniResume :String,
		rate : String,
	},
	mentor2:{
		title :String,
		category : String,
		topics :String,
		description :String
	},
	_isApplied : {
		type : Boolean,
		default : "false"
	},
	_appliedTime : {
		type :String
	},
	upvotes : [{
		type :Schema.Types.ObjectId,
		ref : 'upvotes'
	}],
	answers : [{
		type : Schema.Types.ObjectId,
		ref :  'answers'
	}],
	workExperience : [{
		type : Schema.Types.ObjectId,
		ref :  'workexps'
	}],
	userSkills : [{
		type : Schema.Types.ObjectId,
		ref :  'userskills'
	}],
	file : {
		type : String,
		default : "profile-img.jpg"
	},
	originalfile : {
		type : String
	}
});
module.exports = mongoose.model('users', userSchema);

