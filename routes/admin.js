var express = require('express');
var router = express.Router();
var passport = require('./auth.js');
var mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var multer = require('multer');
var nodemailer = require('nodemailer');
var async = require('async');
var crypto = require('crypto');
var utf8 = require('utf8');
var replaceall = require("replaceall");

//Model Imports
var users = mongoose.model('users');
var skills = mongoose.model('skills');
var posts = mongoose.model('posts');
var events = mongoose.model('events');
var links = mongoose.model('links');
var mentors = mongoose.model('mentors');

var website = 'localhost:3000'
// create reusable transporter object using SMTP transport

var transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
        user: 'nikhil271995',
        pass: 'nik271995'
    }
});

var fileUpload = false;
var fileFilter = false;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
   if (file.fieldname == 'attach') {
    var dir = __dirname;
    var arr = dir.split("routes");
    var addr = arr[0]+"public/uploads";
    console.log(dir,arr,addr);
      cb(null, addr)
    }
  },
  filename: function (req, file, cb) {
    cb(null, req.user  + Date.now() + path.extname(file.originalname))
  }
})

var fileFilter = function(req,file,cb){
  fileUpload = true;
  console.log(file);
  if (file.mimetype=="image/jpeg"||file.mimetype=="image/png"||file.mimetype=="image/jpg") {
    cb(null,true);
  } else {
    fileFilter = true;
    cb(null,false);
  }
}

var uploads = multer({
  storage:storage,
  fileFilter:fileFilter,
  limits:{
    fileSize: 2048*1024
  }
});

var upload = uploads.fields([{ name: 'attach' }]);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('admin/login.html',{'user':req.session.user});
});

router.get('/login', function(req, res, next) {
  	res.render('admin/login.html',{'user':req.session.user});
});

router.get('/home',adminValidate, function(req, res, next) {
  	res.render('admin/index.html',{'user':req.session.user});
});

router.get('/users/:id',adminValidate, function(req, res, next) {
    users.findById(req.params.id,function(err,data){
      res.render('admin/userProfile.html',{'user':req.session.user,'data':data});
    })
});

router.post('/adminlogin', passport.authenticate('adminslogin', {
    successRedirect: '/admin/home',
    failureRedirect: '/admin/login',
    failureFlash:true
}));

router.get('/addskill', adminValidate , function(req,res,next){
  users.find({"_isMentor":"true"},function(err,mentor){
	 res.render('admin/newSkill.html',{'user':req.session.user,'mentor':mentor});
  })
});

router.get('/skills/:id', adminValidate , function(req,res,next){
  skills.findById(req.params.id,function(err,data){
    users.find({"_isMentor":"true"},function(err,mentor){
    res.render('admin/editSkill.html',{'user':req.session.user,'data':data,'mentor':mentor});
    })
  })
});

router.post('/skills/:id', adminValidate , function(req,res,next){
  skills.findByIdAndUpdate(req.params.id,{
    $set : {
      name : req.body.name,
      description : req.body.description,
      reason : req.body.reason,
      category : req.body.category,
      mentorName : req.body.mentorName,
      about : req.body.about,
      rating : req.body.rating
    }
  },function(err,data){
    res.redirect('/admin/viewSkills');
  })
});

router.get('/events',adminValidate,function(req,res,next){
  events.find({},function(err,data){
    console.log(data);
    res.render('admin/viewEvents.html',{'user':req.session.user,'events':data});
  })
});

router.get('/events/:id',adminValidate,function(req,res,next){
  events.findById(req.params.id,function(err,data){
    console.log(data);
    res.render('admin/editEvent.html',{'user':req.session.user,'events':data});
  })
});

router.get('/addEvent', adminValidate , function(req,res,next){
   res.render('admin/newEvent.html',{'user':req.session.user});
});

router.post('/events/:id', adminValidate , function(req,res,next){
  console.log(req.body);
  events.findByIdAndUpdate(req.params.id,{
    $set : {
      name : req.body.name,
      brief : req.body.brief,
      description : req.body.description,
      address : req.body.address,
      timing : req.body.timing,
      pricing : req.body.pricing,
      user : req.user._id,
      lastDate : req.body.lastDate,
      url : req.body.url,
      tags : req.body.tags,
      category : req.body.category,
      currency : req.body.currency
    }
  },function(err,data){
    if(err){
      console.log(err);
      req.flash('error',"Error encountered while saving data.");
    }
    else{
      req.flash("success","Event Data successfully Updated");
    }
    res.redirect('/admin/events/'+req.params.id);
  })
});

router.post('/skills',adminValidate, function(req,res,next){
  if(req.body)
  {
      var skill = new skills({
			name : req.body.name,
			description : req.body.description,
			reason : req.body.reason,
			category : req.body.category,
			mentorName : req.body.mentorName,
      about : req.body.about,
			rating : req.body.rating
		});
    skill.save(function(err, skill) {
			if (err){
				console.log(err);
				req.flash('error','Database Error. Please Try again or Contact Admin if it persists.');
				res.redirect('/admin/viewSkills');
			}
			else{
        req.flash('success','successfully inserted.');
        res.redirect('/admin/viewSkills');
      }
    });
  }
});

router.post('/events',adminValidate, function(req,res,next){
  upload(req, res, function (err) {
    var userId = req.session.user._id;
    var file = false;
      if (err) {
        console.log(err.message);
        req.flash('error',err.message.toString());
        res.redirect('/admin/addEvent');
      }
      else if(fileFilter==true){
        fileFilter = false;
        req.flash('error','Error Uploading Document. Invalid File-Type.');
          res.redirect('/admin/addEvent');
      }
      else if(fileUpload==true && req.files.attach==null){
        fileUpload = false;
        req.flash('error','Error Uploading Document. Max File-Size Exceeded.');
          res.redirect('/admin/addEvent');
      }
      else {
        if(req.body)
        {   
            var originalname,name;
            if (req.files.attach!=null) {
              name = req.files.attach[0].filename;
              originalname = req.files.attach[0].originalname;
            } 
            else {
              originalname=null;
              name=null;
            }
            console.log("i am here - - - -",name,originalname);
            var name = req.body.name;
            name = replaceall(" ","-",name);
            name = utf8.encode(name);
            console.log(name);
            // console.log(req.body)
            links.findOne({"name":name},function(err,data){
              var link;
              console.log(data,"existing link data")
              if(data){
                  links.update({"name":name},{
                    $inc : {
                      count : 1
                    }
                  },function(err,updatedData){
                    if(err){
                      console.log(err);
                    }
                    else {
                      var count = data.count +1;
                      // console.log(count);
                      // console.log(typeof(count.toString()),"typeof");
                      count = count.toString();
                      // console.log(count);
                      name = name+"_"+count;
                      var event = new events({
                        name : req.body.name,
                        brief : req.body.brief,
                        description : req.body.description,
                        address : req.body.address,
                        timing : req.body.timing,
                        pricing : req.body.pricing,
                        user : req.user._id,
                        lastDate : req.body.lastDate,
                        url : req.body.url,
                        tags : req.body.tags,
                        category : req.body.category,
                        currency : req.body.currency,
                        link : name,
                        file : req.files.attach[0].filename,
                        orignalfile : req.files.attach[0].originalname
                      });
                      event.save(function(err,eventData){
                        if(err){
                          console.log(err);
                        }
                        else {
                          console.log(eventData,"eventData");
                          link = new links({
                            name : name,
                            event : eventData._id.toString(),
                            _active : true,
                            count : 1,
                            time : moment().format('MMMM Do YYYY, h:mm:ss a')
                          });
                          link.save(function(err,newdata){
                            if(err)
                            {
                              console.log(err);
                              req.flash('error','Database Error. Please Try again or Contact Admin if it persists.');
                            }
                            else{
                              console.log(newdata,"new link created successfully");
                              req.flash('success','successfully inserted.');
                            }
                          });
                          console.log(eventData,"event successfully created");
                        }
                      });
                    }
                  });
              }
              else{
                var event = new events({
                  name : req.body.name,
                  brief : req.body.brief,
                  description : req.body.description,
                  address : req.body.address,
                  timing : req.body.timing,
                  pricing : req.body.pricing,
                  user : req.user._id,
                  lastDate : req.body.lastDate,
                  url : req.body.url,
                  tags : req.body.tags,
                  category : req.body.category,
                  currency : req.body.currency,
                  link : name,
                  file : req.files.attach[0].filename,
                  orignalfile : req.files.attach[0].originalname
                });
                event.save(function(err,eventData){
                  if(err){
                    console.log(err);
                  }
                  else {
                    console.log(eventData._id.toString());
                    link = new links({
                      name : name,
                      event : eventData._id.toString(),
                      _active : true,
                      count : 1,
                      time : moment().format('MMMM Do YYYY, h:mm:ss a')
                    });
                    link.save(function(err,newdata){
                      if(err)
                      {
                        console.log(err);
                        req.flash('error','Database Error. Please Try again or Contact Admin if it persists.');
                      }
                      else{
                        console.log(newdata,"new link created successfully");
                        req.flash('success','successfully inserted.');
                      }
                    });
                    console.log(eventData,"event successfully created");
                  }
                });  
              }
            });
        }
        else {
        }
        res.redirect("/admin/addEvent");
      }
    });
});

router.get('/viewUsers', adminValidate , function(req,res,next){
  users.find({}).populate('skills').exec(function(err,userData){
    res.render('admin/viewUsers.html',{'user':req.session.user,'users':userData});
  })
});

router.get('/viewSkills', adminValidate , function(req,res,next){
  skills.find({}).populate('skills users').exec(function(err,userData){
    res.render('admin/viewSkills.html',{'user':req.session.user,'skills':userData});
  })
});

router.get('/blog/:id', adminValidate , function(req,res,next){
  users.findById(req.params.id).populate('blog').exec(function(err,data){
    res.render('admin/blog.html',{'user':req.session.user,'data':data});
  })
});

router.get('/editBlog/:id', adminValidate , function(req,res,next){
  posts.findById(req.params.id,function(err,data){
    res.render('admin/editBlog.html',{'user':req.session.user,'data':data});
  });
});

router.post('/editBlog/:id', adminValidate , function(req,res,next){
  posts.findByIdAndUpdate(req.params.id,{
    $set :{
      heading :req.body.heading,
      subheading :req.body.subheading,
      content : req.body.content
    }
  },function(err,userData){
    res.render('admin/editBlog.html',{'user':req.session.user,'data':userData});
  })
});

router.get('/users/admin/:val/:id', adminValidate , function(req,res,next){
  var status = false;
  console.log(req.params.val,"for log");
  if (req.params.val == 1)
     status =true;
  console.log(req.params.id);
  users.findByIdAndUpdate(req.params.id,{
    $set :{
      _isAdmin : status
    }
  },function(err,userData){
    console.log(err,"err\n",userData,"userData");
    res.redirect('/admin/viewUsers');
  })
});

router.get('/users/mentor/:val/:id', adminValidate , function(req,res,next){
  var status = false;
  if (req.params.val == "1")
     status =true;
  users.findByIdAndUpdate(req.params.id,{
    $set :{
      _isMentor : status
    }
  },function(err,userData){
    if(req.params.val == "1")
    {
       var mailOptions = {
            from: 'admin@skillMaker.com', // sender address
            to: userData.email, // list of receivers
            subject: 'Welcome to Skill Maker', // Subject line
            text: '<h2>We have good news for you!</h2> '+
            '<br>Your appliction for being a mentor has been approved and Now you are a mentor.<br>Now you can write answers for questions as well on the Q&A Forum.<br><b>Regards <br>SkillMaker</b> ',
            html: '<h2>>We have good news for you!</h2> '+
            '<br>Your appliction for being a mentor has been approved and Now you are a mentor.<br>Now you can write answers for questions as well on the Q&A Forum.<br><b>Regards <br>SkillMaker</b>'
        };
          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              console.log(error);
              transporter.on('log', console.log);
            }
            else{
              console.log('Message sent : ' + info);
            }
          }); 
    }
    res.redirect('/admin/viewUsers');
  })
});

var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

function adminValidate(req,res,next){
	//console.log(req.user);
	users.findById(req.user,function(err, user) {
		if(user && user._isAdmin){
			req.session.user = user;
      res.locals.user=user;
			next();
		}
		else {
      		res.redirect('/admin/login');
		}
	});
}

module.exports = router;