var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var skillSchema = new Schema({
	name:{
		type: String,
		default : ""
	},
	description:{
		type: String,
		default : ""
	},
	category:{
		type:String,
		default :""
	},
	mentorName : {
		type: Schema.Types.ObjectId,
		ref : "users"	
	},
	reason :{
		type :String,
	},
	studentCount : Number,
  	rating : {
		type: Number,
		default : String
	},
  	image : {
		type : String,
		default : ""
	},
	type : {
		type:String,
		default : "free"
	},
	duration : {
		type : String ,
		default : ""
	},
	certificates : {
		type : String,
		default : "no"
	},
	lessonsNumber : {
		type :Number
	},
	mentors : [{
		type: Schema.Types.ObjectId,
		ref: 'users'
	}],
	features : [{
		details: String
	}],
	lessons : [{
		info: String,
		duration : String
	}],
	about : {
		type:String,
		default : ""
	},
	images : {
	 type : String,
	 default : "course-2.jpg"
	}
});
module.exports = mongoose.model('skills', skillSchema);
