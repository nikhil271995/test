var passport = require('passport'),
LocalStrategy   = require('passport-local').Strategy;
var mongoose = require('mongoose');
var users = mongoose.model('users');
var bCrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');
var moment = require('moment');

// User
passport.serializeUser(function(user, done) {
        console.log('serializing user..');
        done(null, user._id);
});

passport.deserializeUser(function(obj, done) {
  //console.log("deserializing " + obj);
  done(null, obj);
});

passport.use('userlogin',new LocalStrategy(
    function(username, password, done) {
        users.findOneAndUpdate({ 'email' :  username },{
                 $set : {
                        lastLogin : moment().date()
                    }
                },
            function(err, user) {
                console.log(moment().toDate().getTime());
                if (err)
                    return done(err);
                if (!user){
                  console.log("incorrect userid");
                    return done(null, false, { message: 'Incorrect Username/Password. Please try again.' });
                }
                if (!isValidPassword(user, password)){
                    console.log("invalid pas");
                    return done(null, false, { message: 'Incorrect Password. Please try again.' });
                }
                // if(!user._email){
                //     console.log("Email not verified");
                //     return done(null, false, { message: 'Please Verify your email to Login.' });
                // }
                // if(!user._login){
                //   console.log("Login Disabled");
                //     return done(null, false, { message: 'Login Disabled for this user.Contact Admin for support.' });
                // }
                return done(null, user);
            }
        );

    })
);

passport.use('adminslogin',new LocalStrategy(
    function(username, password, done) {
        users.findOneAndUpdate({ 'email' :  username },{
                 $set : {
                        lastLogin : moment().date()
                    }
                },
            function(err, user) {
                console.log(moment().toDate().getTime());
                if (err)
                    return done(err);
                if (!user){
                  console.log("incorrect");
                    return done(null, false, { message: 'Incorrect Username/Password. Please try again.' });
                }
                if (!isValidPassword(user, password)){
                    return done(null, false, { message: 'Incorrect Password. Please try again.' });
                }
                if(!user._isAdmin){
                  return done(null,false,{message : 'You are not an admin. Plase contact admin for getting this privlege.'});
                }
                // if(!user._email){
                //      console.log("Email not verified");
                //      return done(null, false, { message: 'Please Verify your email to Login.' });
                // }
                // if(!user._login){
                //   console.log("Login Disabled");
                //     return done(null, false, { message: 'Login Disabled for this user.Contact Admin for support.' });
                // }
                return done(null, user);
            }
        );

    })
);

var isValidPassword = function(user, password){
	return bCrypt.compareSync(password, user.password);
}

module.exports = passport;
