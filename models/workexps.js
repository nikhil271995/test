var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var workexpSchema = new Schema({
 name : String,
 duration : String,
 role : String,
 roleDescription : String,
 others : String	
});

module.exports = mongoose.model('workexps',workexpSchema);

