var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var upvoteSchema = new Schema({
  user : {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  answer : {
    type :Schema.Types.ObjectId ,
    ref : 'answers',
  },
  question : {
    type:Schema.Types.ObjectId,
    ref : 'questions'
  },
  status : {
    type : Boolean,
    default :false
  },
  time : String
});
module.exports = mongoose.model('upvotes',upvoteSchema);

