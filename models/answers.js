var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var answerSchema = new Schema({
user : {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  answer : {
    type :String ,
    required : true
  },
  time : {
    type:String
  },
  upvotes : {
    type :Number,
    default : 0
  },
  commentCount :{
    type : Number
  },
  comments :[{
    type : Schema.Types.ObjectId,
    ref: 'comments' 
  }],
  question:{
    type :Schema.Types.ObjectId,
    ref : 'questions'
  }
});
module.exports = mongoose.model('answers',answerSchema);
