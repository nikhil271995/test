var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userskillsSchema = new Schema({
 name: String,
 level: Number,
 projectUrl : String,
 team : String,
 projectDescription : String	
});

module.exports = mongoose.model('userskills',userskillsSchema);

