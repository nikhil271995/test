var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mentorSchema = new Schema({
event : {
      type: Schema.Types.ObjectId,
      ref: 'users'
  },
name:{
  type : String,
  require : true
},
_active : {
  type :Boolean,
  default : true
},
time : {
  type:String
},
count : {
	type : Number,
	default : 1
}
});
module.exports = mongoose.model('mentors',mentorSchema);

