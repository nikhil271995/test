var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var questionSchema = new Schema({
question :{
  type:String,
  required :true
},
details:{
  type:String,
  required :true
},
topics:{
  type:String,
  required :true
},
time :{
  type : String
},
mode: {
  type : Boolean,
  default :false
},
askedBy :{
  type: Schema.Types.ObjectId,
  ref: 'users'
},
answers :[{
  answer : {
    type : Schema.Types.ObjectId,
    ref : 'answers'
  },
  user :{
    type :Schema.Types.ObjectId,
    ref : 'users'
  }
}],
answerCount:{
  type:Number,
  default : 0
}
});
module.exports = mongoose.model('questions', questionSchema);
