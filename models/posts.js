var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var postSchema = new Schema({
  heading:String,
  subheading:String,
  content:String,
  _file :{
    type : Boolean,
    default :false
  },
  file : {
    type: String
  },
  orignalfile : {
    type : String
  },
  user : {
    type: Schema.Types.ObjectId,
    ref : 'users'
  },
  timestamp : String
});
module.exports = mongoose.model('posts',postSchema);
