var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var commentSchema = new Schema({
user : {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    comment : {
      type :String ,
      required : true,
    },
    time : {
      type:String
    }
});
module.exports = mongoose.model('comments',commentSchema);

