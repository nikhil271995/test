var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var educationSchema = new Schema({
  level : String,
  degree :String,
  major :String,
  university : String,
  college :String,
  from :String,
  to :String,
  backlogs :String,
  percent :String
});
module.exports = mongoose.model('usereducations',educationSchema);

