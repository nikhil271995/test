var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var eventSchema = new Schema({
createdBy : {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    name : {
      type :String ,
      required : true,
    },
    brief : {
      type: String,
      required :true
    },
    timing : {
      type:String,
      required : true
    },
    pricing : {
      type :String,
      required : true
    },
    status : {
      type : Boolean,
      default : false
    },
    address : {
      type :String,
      required : true
    },
    description : {
      type : String,
      required : true
    },
    category:{
      type : String
    },
    image : {
      type : String
    },
    lastDate :{
      type : String
    },
    url :{
      type : String
    },
    tags :{
      type : String
    },
    link :{
      type: String,
      required : true
    },
    currency :{
      type : String
    },
    file : {
      type : String
    },
    orignalfile : {
      type : String
    }
});
module.exports = mongoose.model('events',eventSchema);

