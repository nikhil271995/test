var express = require('express');
var router = express.Router();
var passport = require('./auth.js');
var mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var nodemailer = require('nodemailer');
var async = require('async');
var crypto = require('crypto');

//Mongo Models
var users = mongoose.model('users');
var posts = mongoose.model('posts');
var skills = mongoose.model('skills');
var questions = mongoose.model('questions');
var upvotes = mongoose.model('upvotes');
var events = mongoose.model('events');
var usereducations = mongoose.model('usereducations');
var userskills = mongoose.model('userskills');
var workexps = mongoose.model('workexps');

var website = 'http://skillmaker.org';
// create reusable transporter object using SMTP transport

var transporter = nodemailer.createTransport({
    service: 'SendGrid',
    auth: {
        user: 'nikhil271995',
        pass: 'nik271995'
    }
});

var fileUpload = false;
var fileFilter = false;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
   if (file.fieldname == 'attach') {
      cb(null, '/home/anv13/Documents/git/skillMaker/public/uploads')
    }
  },
  filename: function (req, file, cb) {
    cb(null, req.user  + Date.now() + path.extname(file.originalname))
  }
})

var fileFilter = function(req,file,cb){
	fileUpload = true;
	console.log(file);
	if (file.mimetype=="image/jpeg"||file.mimetype=="image/png"||file.mimetype=="image/jpg") {
		cb(null,true);
	} else {
		fileFilter = true;
		cb(null,false);
	}
}

var uploads = multer({
	storage:storage,
	fileFilter:fileFilter,
	limits:{
		fileSize: 2048*1024
	}
});

var upload = uploads.fields([{ name: 'attach' }]);

/* GET home page. */
router.get('/', function(req, res, next) {
  	res.redirect('/users/home',{'user':req.session.user });
});

router.get('/users',function(req,res,next){
    res.redirect('/users/home',{'user':req.session.user });
})

router.get('/home', function(req, res, next) {
    users.find({"_isMentor":"true"}).limit(3).exec(function(err,mentorData){
    res.render('users/home.html',{'user':req.session.user,'mentorData':mentorData});
  })
});

router.get('/login', function(req, res, next) {
  	res.render('users/signin.html',{'user':req.session.user});
});

router.get('/events', function(req, res, next) {
    events.find({},function(err,data){
        res.render('users/events.html',{'user':req.session.user,'events':data});
    })
});

router.get('/events/:id', function(req, res, next) {
    res.render('users/register.html',{'user':req.session.user});
});

router.get('/viewEvents/:id', function(req, res, next) {
      events.findById(req.params.id,function(err,data){
        res.render('users/viewEvents.html',{'user':req.session.user,'data':data});
      });
});

router.get('/skills/:id', function(req, res, next) {
    skills.findById(req.params.id,function(err,data){
      res.render('users/skills.html',{'user':req.session.user,'skill':data});
    })
});

router.post('/search',function(req,res,next){
  var search = req.body.search;
  var arr =  search.split(" ");
  console.log("here",arr);
  var index = function(db, callback) {
   db.collection('skills').createIndex(
      {
        "name" :1
      },
      function(err, results) {
         console.log(results);
         callback();
      }
   );
};
  skills.find({ $text: { $search: search } },function(err,data){
    console.log(data);
    res.render('users/search.html',{'user':req.session.user,"data":data});
  });
})

router.get('/forgot', function(req, res, next) {
  	res.render('users/forget.html',{'user':req.session.user});
});

router.get('/confirm/:id', function(req, res, next) {
    users.findByIdAndUpdate(req.params.id,{
      $set : {
        _email : true
      }
    },function(err,data){
      req.flash("Email Verified");
      res.redirect("/");
    });
});

router.get('/delete/:type/:id', userValidate, function(req, res, next) {
  var type = req.params.type;
  var id = req.params.id;
  console.log("type",type);
  if(type == "educations")
  {
    usereducations.remove({'_id':id},function(err,data){
      console.log(err);
    });
    console.log("here");
    users.findByIdAndUpdate(req.session.user._id,{
      $pull : {
        education : id
      }
    },function(err,data){
      console.log(err);
    })
  }
  if(type == "userskills")
  {
    userskills.remove({'_id':id},function(err,data){
      console.log(err);
    });
    users.findByIdAndUpdate(req.session.user._id,{
      $pull : {
        userSkills : {
           $in: id
        } 
      }
    },function(err,data){
      console.log(err);
    })
  }
  if(type == "workexps")
  {
    workexps.remove({'_id':id},function(err,data){
      console.log(err);
    });
    users.findByIdAndUpdate(req.session.user._id,{
      $pull : {
        workExperience : {
           $in: id
        } 
      }
    },function(err,data){
      console.log(err);
    })
  }
    res.redirect('/users/makers');
});

router.get('/makers',userValidate, function(req, res, next) {
  // console.log(req.session);
  // console.log(req.user,"user");
  users.findById(req.user).populate('education workExperience userSkills').exec(function(err,data){
    console.log(data);
    res.render('users/makersProfile.html',{'user':req.session.user,'data':data});
  })
});


router.post('/addEducation',userValidate, function(req, res, next) {
  // console.log(req.body);
    var newed = new usereducations({
        level: req.body.level,
            degree: req.body.degree,
            major: req.body.major,
            university: req.body.university,
            college: req.body.college,
            from: req.body.from,
            to: req.body.to,
            backlogs: req.body.backlogs,
            percent: req.body.percent 
      });
    newed.save(function(err,eddata){
      if(err){
        console.log(err);
        res.redirect('/users/makers')
      }
      else{
        console.log(eddata._id,"here","eddata",eddata);
        users.findByIdAndUpdate(req.session.user._id,{
          $push : {   
                education : eddata._id.toString()
            }
          },function(error,data){
            res.redirect('/users/makers');
          })
        }
    })
    
});

router.post('/addExperience',userValidate, function(req, res, next) {
  console.log(req.body);
    var newed = new workexps({
      name: req.body.name,
      duration: req.body.duration,
      role: req.body.role,
      roleDescription: req.body.roleDescription,
      others: req.body.others
    });
    newed.save(function(err,newdata){
      users.findByIdAndUpdate(req.session.user._id,{
      $push : {
            workExperience : newdata._id.toString()
        }
      },function(error,data){
        res.redirect('/users/makers');
      });
    });
});

router.post('/addSkills',userValidate, function(req, res, next) {
  console.log(req.body);
    var newed = new userskills({
      name : req.body.name,
      level : req.body.level,
      projectUrl : req.body.projectUrl,
      projectDescription : req.body.projectDescription,
      team :req.body.team
    });
    newed.save(function(err,newdata){
      users.findByIdAndUpdate(req.session.user._id,{
        $push : {
              userSkills : newdata._id.toString()
          }
        },function(error,data){
          res.redirect('/users/makers');
        })      
    })
    
});

router.post('/editinfo',userValidate, function(req, res, next) {
  console.log(req.body);
    users.findByIdAndUpdate(req.session.user._id,{
    $set : {
        about: req.body.about,
        facebook: req.body.facebook,
        twitter: req.body.twitter,
        linkedin: req.body.linkedin,
        github: req.body.github,
        phone : {
          mobile : req.body.mobile
        },
        address : req.body.address
      }
    },function(error,data){
      res.redirect('/users/makers');
    })
});

router.post('/userslogin', passport.authenticate('userlogin', {
    successRedirect: '/users/makers',
    failureRedirect: '/users/home',
    failureFlash:true
}));

router.post('/userSignup',function(req,res,next)
{
	console.log("reqbody",req.body);
  	users.findOne({'email':req.body.email},function(err, user) {
  		if(user!=null){
  			res.send({registrationError:'E-mail already registered. Please Register using a different E-mail or use Forgot Password for password recovery.'});
  		}
  		else{
		  // Database Entry
		  var user = new users({
		  	name:req.body.name,
		  	email:req.body.email,
		  	password:createHash(req.body.password),
		  });
		  user.save(function(err, user) {
		  	if (err){
		  		console.log(err);
		  		res.send({message:'Database Error. Please Try again or Contact Admin if it persists.'});
		  	}
		  	else{
		  		// res.send({messages:' Registration is Successfull. Check your mail to verify you account.<br>Thank You'});
          res.redirect("/");
		  	var mailOptions = {
				    from: 'admin@skillMaker.org', // sender address
				    to: req.body.email, // list of receivers
				    subject: 'Welcome to Skill Maker', // Subject line
				    text: '<h2>Welcome to SkillMaker</h2>Hello '+req.body.name+
				    '<br>Thank You for signing up for SkillMaker. You can login after verifying your email by clicking '+
				    '<a href="'+website+'/users/confirm/'+user._id+'">here</a>.<br>'+ 'You can also paste the link below in your browser to confirm.<br>'+
				    '<a href="'+website+'/users/confirm/'+user._id+'">'+website+'/users/confirm/'+user._id+'</a><br><br>Regards,<br>Webmaster<br>ConfluenceEdu', // plaintext body
				    html: '<h2>Welcome to SkillMaker</h2>Hello '+req.body.name+
				    '<br>Thank You for signing up for SkillMaker. You can login after verifying your email by clicking '+
				    '<a href="'+website+'/users/confirm/'+user._id+'">here</a>.<br>'+ 'You can also paste the link below in your browser to confirm.<br>'+
				    '<a href="'+website+'/users/confirm/'+user._id+'">'+website+'/users/confirm/'+user._id+'</a><br><br>Regards,<br>Webmaster<br>SkillMaker' // html body
				};
  				transporter.sendMail(mailOptions, function(error, info){
  					if(error){
  						console.log(error);
              transporter.on('log', console.log);
  					}else{
  						console.log('Message sent : ' + info);
  					}
  				});
		  	}
		  });
		}
	});
});

router.get('/profile', userValidate , function(req,res,next){
  users.findById(req.session.user._id).populate('education workExperience userSkills').exec(function(err,data){
      res.render('users/makersProfile.html',{'user':req.session.user,'data':data});
  })
});

router.get('/editProfile', userValidate , function(req,res,next){
	res.render('users/profileEdit.html',{'user':req.session.user});
});

router.get('/profile/basic', userValidate , function(req,res,next){
	res.render('users/profileBasic.html',{'user':req.session.user});
});

router.post('/profile/basic', userValidate , function(req,res,next){
  users.findByIdAndUpdate(req.session.user._id,{
    $set : {
      username : req.body.username,
      altEmail : req.body.altEmail,
      address : req.body.address,
      phone : {
        mobile : req.body.phone
      }
    }
  },function(err,user){
    console.log(user);
    res.redirect('/users/profile/basic');
  })
});

router.get('/education', userValidate , function(req,res,next){
	res.render('users/education.html',{'user':req.session.user});
});

router.get('/education/new', userValidate , function(req,res,next){
	res.render('users/educationNew.html',{'user':req.session.user});
});

router.post('/education/new', userValidate , function(req,res,next){
 var _id;
  crypto.randomBytes(64, (err, buf) => {
      if (err) throw err;
      _id = buf;
      // console.log(`${buf.length} bytes of random data: ${buf.toString('hex')}`);
      users.findByIdAndUpdate(req.session.user._id,{
        $push : {
          education : {
            _id : buf.toString('hex'),
            start : req.body.start,
            end :  req.body.end,
            school : req.body.school,
            qualification : req.body.qualification,
            notes : req.body.notes
          }
        }
      },function(err,user){
        // console.log(user);
        res.redirect('/users/education');
      })
  });
});

router.get('/profile/skills', userValidate , function(req,res,next){
	res.render('users/skills.html',{'user':req.session.user});
});

router.get('/apply', userValidate , function(req,res,next){
	res.render('users/apply.html',{'user':req.session.user});
});

router.post('/apply', userValidate , function(req,res,next){
  users.findByIdAndUpdate(req.session.user._id,{
    $set : {
      mentor1 : {
            shortBio : req.body.shortBio,
            miniResume : req.body.miniResume,
            rate : req.body.rate
      }
    }
  },function(err,user){
    console.log(user);
    res.redirect('/users/apply/skills');
  })
});

router.get('/apply', userValidate , function(req,res,next){
	res.render('users/apply.html',{'user':req.session.user});
});

router.post('/apply/profile', userValidate , function(req,res,next){
  users.findByIdAndUpdate(req.session.user._id,{
    $set : {
      mentor1 : {
            shortBio  : req.body.shortBio,
            miniResume : req.body.miniResume,
            rate : req.body.rate
      }
    }
  },function(err,user){
    console.log(user);
    res.redirect('/users/apply/profile');
  })
});

router.get("/addBlog",userValidate,function(req,res,next){
  users.findById(req.session.user._id,function(err,data){
    res.render('users/addBlog.html',{'user':req.session.user,'data':data});
  });
});

router.get('/apply/skills', userValidate , function(req,res,next){
	res.render('users/applySkills.html',{'user':req.session.user});
});

router.post('/apply/skills', userValidate , function(req,res,next){
  users.findByIdAndUpdate(req.session.user._id,{
    $set : {
      mentor2 : {
            description  : req.body.description,
            topics : req.body.topics,
            category : req.body.category,
            title : req.body.title
      },
      _isApplied : "true",
      _appliedTime :  Date.now()
    }
  },function(err,user){
    console.log(user);
    if(err)
      console.log(err);
    else 
    {
      var mailOptions = {
            from: 'admin@skillMaker.com', // sender address
            to: req.session.user.email, // list of receivers
            subject: 'Welcome to Skill Maker', // Subject line
            text: '<h2>We have receieved your Request for being a mentor</h2> '+
            '<br>We will get back to you soon. You will get a mail about after your request has been analysed. ',
            html: '<h2>>We have receieved your Request for being a mentor.</h2> '+
            '<br>We will get back to you soon. You will get a mail about after your request has been analysed.This may take some time .<br><br><b>Regards <br>SkillMaker</b>'
        };
          transporter.sendMail(mailOptions, function(error, info){
            if(error){
              console.log(error);
              transporter.on('log', console.log);
            }else{
              console.log('Message sent : ' + info);
            }
          }); 
    }
    res.redirect('makers');
  })
});

router.get("/addBlog",userValidate,function(req,res,next){
  users.findById(req.session.user._id,function(err,data){
    res.render('users/addBlog.html',{'user':req.session.user,'data':data});
  });
});

router.post('/addBlog',userValidate,function(req, res, next) {
	upload(req, res, function (err) {
		var userId = req.session.user._id;
		var file = false;
	    if (err) {
	      console.log(err.message);
	      req.flash('error',err.message.toString());
	      res.render('users/addBlog.html',{'user':req.session.user});
	    }
	    else if(fileFilter==true){
	    	fileFilter = false;
	    	req.flash('error','Error Uploading Document. Invalid File-Type.');
	      res.render('users/addBlog.html',{'user':req.session.user});
	    }else if(fileUpload==true && req.files.attach==null){
	    	fileUpload = false;
	    	req.flash('error','Error Uploading Document. Max File-Size Exceeded.');
        res.render('users/addBlog.html',{'user':req.session.user});	    }
      else {
			var message = req.body.message;
			var originalname,name;
			if (req.files.attach!=null) {
				file = true;
				name = req.files.attach[0].filename;
				originalname = req.files.attach[0].originalname;
			} else {
				originalname=null;
				name=null;
			}
			var post = new posts({
            heading: req.body.heading,
		  		  subheading: req.body.subheading,
            content : req.body.content,
        		timestamp: moment().format('MMMM Do YYYY, h:mm:ss a'),
        		user:req.session.user._id,
        		_file : file ,
        		file : name,
        		orignalfile : originalname
		  	});
		  	post.save(function(err, post) {
		  	if (err){
		  		console.log(err);
		  		req.flash('error','Database Error. Please Try again or Contact Admin if it persists.');
		  		res.render('users/addBlog.html',{'user':req.session.user});
		  	}
		  	else{
          console.log(post);
          users.findByIdAndUpdate(req.session.user._id,{
            $push:{
              blog : post._id
            }
          },function(err,user){
            console.log("Updated users",user)
          });
          res.render('users/addBlog.html',{'user':req.session.user});
		  	}
		  });
		}
	});
});

router.get("/manageBlog",userValidate,function(req,res,next){
  users.findById(req.session.user._id).populate('blog').exec(function(err,data){
    res.render('users/viewBlog.html',{'user':req.session.user,'data':data});
  });
});

router.get("/dashboard",userValidate,function(req,res,next){
  users.findById(req.session.user._id,function(err,data){
    res.render('users/dashboard.html',{'user':req.session.user});
  });
});

router.post('/dashboardData', userValidate , function(req,res,next){
  users.findById(req.session.user._id).populate("answers").exec(function(err,user) {
    async.forEach(user.answers,function(answer,callback) {
        questions.populate(
          answer,
          { path: "question" },
        function(err,output) {
          if (err) throw err;
          callback();
        });
      },function(err) {
      // console.log( JSON.stringify( user, undefined, 4) );
      res.send(user);
    });
  });
});

router.get('/signout', function(req, res) {
  req.logout();
    req.session.destroy()
  res.redirect('/');
});

var createHash = function(password){
	return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

function userValidate(req,res,next){
	//console.log(req.user);
	users.findById(req.user,function(err, user) {
		if(user!=null){
			req.session.user = user;
      res.locals.user=user;
			next();
		}
		else {
      		res.redirect('/users/home');
		}
	});
}

module.exports = router;
